package com.ts.spacegame.viewModels

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.*
import com.ts.spacegame.R
import com.ts.spacegame.models.Astronaut
import com.ts.spacegame.models.GameObject
import com.ts.spacegame.models.MyModel

/**
 * The class [AstronautView] is used to generate a view for the second mission.
 * it extends the class [SurfaceView]
 * it sets up movable objects from other classes alongside their colliders.
 * it checks for collision between the player and the other objects.
 * it includes a singleton for relative variables.
 */
@SuppressLint("UseCompatLoadingForDrawables")
class AstronautView(context: Context?, attrs: AttributeSet?) : SurfaceView(context, attrs), Runnable {

    private var rectAsteroid1: Rect = Rect() // collider for asteroid
    private var rectAsteroid2: Rect = Rect() // collider for asteroid
    private var rectAsteroid3: Rect = Rect() // collider for asteroid
    private var rectAsteroid4: Rect = Rect() // collider for asteroid
    private var entered: Boolean = false // to check if player entered the target area
    private var isRunning: Boolean = true // to check if the view is intractable
    private var paint = Paint() // to paint the background of the view
    private var myThread: Thread // to have separate threads for a smooth experience
    private var myHolder: SurfaceHolder // the holder for the view
    private var myGameObjects = ArrayList<GameObject>() // arraylist of the game objects
    private val astronautImage = context!!.resources.getDrawable(R.drawable.astronaut, null) // obtain the image for the astronaut
    private var astronaut = Astronaut(MyModel.widthWindow /2 - 100, MyModel.heightWindow - 1300, 0, 0, astronautImage) // set up position of the astronaut
    private var rectAstronaut: Rect = Rect() // set up a collider for the astronaut

    init {
        if (context != null) {
            // add each asteroid in the list
            myGameObjects.add(GameObject(300, 200, 15, 0, context.resources.getDrawable(R.drawable.meteorite, null))) // obtain and add the asteroid object in the list
            myGameObjects.add(GameObject(0, 400, 20, 0, context.resources.getDrawable(R.drawable.asteroid2, null))) // obtain and add the asteroid object in the list
            myGameObjects.add(GameObject(100, 600, 10, 0, context.resources.getDrawable(R.drawable.asteroid, null))) // obtain and add the asteroid object in the list
            myGameObjects.add(GameObject(200, 800, 30, 0, context.resources.getDrawable(R.drawable.asteroid4, null))) // obtain and add the asteroid object in the list
        }

        myGameObjects.add(astronaut) // add player in the list
        myThread = Thread(this) // set up the thread within the context
        myThread.start() // start thread
        myHolder = holder // controls access to canvas
    }

    /**
     * override function [onTouchEvent] used to move the player around the view.
     * the player is only movable when the game is not in pause mode.
     */
    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent?): Boolean {
        if(!SecondMission.isPaused) {
            if (event != null) {
                if (event.action == MotionEvent.ACTION_MOVE) {
                    astronaut.px = (event.rawX - astronaut.width + 100).toInt() // center the player on touch
                    astronaut.py = (event.rawY - astronaut.height - 1300).toInt() // lift the player to make it visible when dragging
                }
            }
        }
        return true
    }

    /**
     * override function [run] used to make the view intractable for the user.
     * it checks if it is running.
     * it checks if the game is in not pause mode and is started.
     * it locks the canvas to prevent other threads to use this section.
     * it sets up the canvas for the view.
     * update the position of the colliders based on the movement of the objects and player.
     * check if the player collided with an object.
     * it restores the original place of the player if collided with other objects.
     * it checks if the player entered the target area and notify the view.
     */
    override fun run() {
        while(isRunning) // main body of the intractable view
        {
            if(!SecondMission.isPaused && SecondMission.isStarted) {
                if(!myHolder.surface.isValid) {
                    continue
                }

                val canvas: Canvas = myHolder.lockCanvas() // prevent other threads using this section
                canvas.drawRect(0f, 0f, canvas.width.toFloat(), canvas.height.toFloat(), paint) // set up canvas

                for(gameObject in myGameObjects) { // for each game object and player update the collider position relatively
                    gameObject.move(canvas)
                    rectAstronaut = Rect(astronaut.rectPlayer)
                    rectAsteroid1 = Rect(myGameObjects[0].rectObject)
                    rectAsteroid2 = Rect(myGameObjects[1].rectObject)
                    rectAsteroid3 = Rect(myGameObjects[2].rectObject)
                    rectAsteroid4 = Rect(myGameObjects[3].rectObject)
                }

                if (rectAstronaut.intersect(rectAsteroid1) ||
                    rectAstronaut.intersect(rectAsteroid2) ||
                    rectAstronaut.intersect(rectAsteroid3) ||
                    rectAstronaut.intersect(rectAsteroid4)) { // if player collided with any object
                    myGameObjects.removeAt(4) // remove the player
                    astronaut = Astronaut(MyModel.widthWindow /2 - 100, MyModel.heightWindow - 1300, 0, 0, astronautImage) // restore position
                    myGameObjects.add(4, astronaut) // add the player back to the list
                    SecondMission.retry = true // notify the view about the collision
                    SecondMission.isPaused = true // pause the view
                } else if(astronaut.y < (canvas.height - height) - 70) { // if player entered the target area
                    if (entered) { // if enter is true
                        SecondMission.isMissionCompleted = true // notify the view about the completion of the mission
                        isRunning = false // stop the view from running
                    }
                    if (!entered) { // is enter is not true
                        myGameObjects.removeAt(4) // remove player from the list
                        entered = true
                    }
                }
                myHolder.unlockCanvasAndPost(canvas) // unlock the canvas
            }
        }
    }
}

/**
 * singleton object [SecondMission] used to monitor and notify the view.
 */
object SecondMission {
    var isStarted: Boolean = false

    var isMissionCompleted: Boolean = false // to check if completed
    var isMissionEnded: Boolean = false // for stopping run()

    var retry: Boolean = false
    var isPaused: Boolean = false // for try again
}