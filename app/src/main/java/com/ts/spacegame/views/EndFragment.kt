package com.ts.spacegame.views

import android.media.MediaPlayer
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import android.view.animation.RotateAnimation
import android.view.animation.TranslateAnimation
import androidx.core.view.isVisible
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.ts.spacegame.R
import com.ts.spacegame.databinding.FragmentEndBinding
import com.ts.spacegame.models.MyModel

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [EndFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class EndFragment : Fragment() {
    private var param1: String? = null
    private var param2: String? = null
    private lateinit var binding: FragmentEndBinding
    private lateinit var navController: NavController
    private lateinit var animButton: TranslateAnimation // translate animation
    private var backgroundAudio: MediaPlayer = MediaPlayer() // audio
    private var messageAudio: MediaPlayer = MediaPlayer() // audio
    private var buttonAudio: MediaPlayer = MediaPlayer() // audio
    private var swipeAudio: MediaPlayer = MediaPlayer() // audio

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentEndBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // for audio
        if (MyModel.globalAudio) {
            backgroundAudio = MediaPlayer.create(requireActivity(), R.raw.background4)
            backgroundAudio.setVolume(MyModel.backgroundEndAudio, MyModel.backgroundEndAudio)
            backgroundAudio.isLooping = true
            backgroundAudio.start()
            messageAudio = MediaPlayer.create(requireActivity(), R.raw.success1)
            messageAudio.setVolume(MyModel.finalMessageAudio, MyModel.finalMessageAudio)
            buttonAudio = MediaPlayer.create(requireActivity(), R.raw.button1)
            buttonAudio.setVolume(MyModel.buttonVolume, MyModel.buttonVolume)
            swipeAudio = MediaPlayer.create(requireActivity(), R.raw.swipe1)
            swipeAudio.setVolume(MyModel.swipeUpAudio, MyModel.swipeUpAudio)
        }

        binding.cardMessageEnd.isVisible = false // hide the message
        binding.buttonHomeEnd.y = MyModel.heightWindow.toFloat() // bring button below the height of the screen
        binding.astronautEnd.y = MyModel.heightWindow.toFloat() // bring button asset below the height of the screen
        animateFragment()

        // invisible button on the final position of the animated button
        binding.buttonHomeEndGhost.setOnClickListener {
            if (animButton.hasEnded()) {
                binding.buttonHomeEndGhost.isEnabled = false
                navController = findNavController()
                navController.navigate(R.id.action_endFragment_to_homeFragment) // go back to home fragment
                if (MyModel.globalAudio) { // if audio enabled
                    buttonAudio.start()
                    backgroundAudio.stop()
                }
            }
        }
    }

    /**
     * private fun [animateFragment] is sued to animate the asteroids.
     */
    private fun animateFragment() {
        // rotation animation for asteroids
        val animAsteroid = RotateAnimation(0f,359f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f)
        animAsteroid.duration = 15000
        animAsteroid.fillAfter = true
        animAsteroid.repeatCount = Animation.INFINITE
        animAsteroid.setInterpolator(context, android.R.anim.linear_interpolator)
        binding.asteroidEnd1.startAnimation(animAsteroid)
        binding.asteroidEnd2.startAnimation(animAsteroid)

        // alpha animation for final message
        val animCard = AlphaAnimation(0.0f, 1.0f) // final message
        animCard.duration = 1000
        animCard.startOffset = 1000
        animCard.fillAfter = true
        binding.cardMessageEnd.isVisible = true
        binding.cardMessageEnd.startAnimation(animCard)
        if (MyModel.globalAudio) {
            Handler(Looper.getMainLooper()).postDelayed({
                messageAudio.start() // play audio
            }, 1000)
        }

        // translation animation for button
        animButton = TranslateAnimation(0f, 0f, MyModel.heightWindow.toFloat(), -MyModel.heightWindow.toFloat()) // home button
        animButton.duration = 1000
        animButton.startOffset = 1200
        animButton.fillAfter = true
        animButton.repeatCount = 0
        binding.buttonHomeEnd.startAnimation(animButton)
        binding.astronautEnd.startAnimation(animButton)
        if (MyModel.globalAudio) {
            Handler(Looper.getMainLooper()).postDelayed({
                swipeAudio.start()
            }, 1800)
        }
    }

    /**
     * function to pause the audio on stop
     */
    override fun onStop() {
        super.onStop()
        if (MyModel.globalAudio) {
            backgroundAudio.pause()
        }
    }

    /**
     * function to resume the audio on resume
     */
    override fun onResume() {
        super.onResume()
        if (MyModel.globalAudio) {
            backgroundAudio.start()
        }
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment EndFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            EndFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}