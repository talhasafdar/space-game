package com.ts.spacegame.views

import android.media.MediaPlayer
import android.os.*
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ts.spacegame.R
import com.ts.spacegame.databinding.FragmentFirstMissionBinding
import com.ts.spacegame.models.MyModel
import com.ts.spacegame.viewModels.MyViewModel
import com.ts.spacegame.viewModels.PlanetAdapter
import com.ts.spacegame.models.PlanetItem
import java.util.*

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [FirstMissionFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class FirstMissionFragment : Fragment() {
    private var param1: String? = null
    private var param2: String? = null
    private lateinit var binding: FragmentFirstMissionBinding
    private lateinit var navController: NavController
    private lateinit var myViewModel: MyViewModel
    private lateinit var planetAdapter: PlanetAdapter // adapter
    private var displayList = mutableListOf<PlanetItem>() // list of planets
    private lateinit var itemTouchHelper: ItemTouchHelper // touch helper
    private var instructionAudio: MediaPlayer = MediaPlayer() // audio
    private var buttonAudio: MediaPlayer = MediaPlayer() // audio
    private var correctAudio: MediaPlayer = MediaPlayer() // audio
    private var moveAudio: MediaPlayer = MediaPlayer() // audio
    private var backgroundAudio: MediaPlayer = MediaPlayer() // audio
    private var dismissButton: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentFirstMissionBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // for audio
        if (MyModel.globalAudio) {
            backgroundAudio = MediaPlayer.create(requireActivity(), R.raw.background2)
            backgroundAudio.setVolume(MyModel.backgroundEndAudio, MyModel.backgroundEndAudio)
            backgroundAudio.isLooping = true
            instructionAudio = MediaPlayer.create(requireActivity(), R.raw.instruction1)
            instructionAudio.setVolume(MyModel.animationInVolume, MyModel.animationInVolume)
            buttonAudio = MediaPlayer.create(requireActivity(), R.raw.button1)
            buttonAudio.setVolume(MyModel.buttonVolume, MyModel.buttonVolume)
            correctAudio = MediaPlayer.create(requireActivity(), R.raw.correct1)
            correctAudio.setVolume(MyModel.correctAudio, MyModel.correctAudio)
            moveAudio = MediaPlayer.create(requireActivity(), R.raw.move3)
            moveAudio.setVolume(MyModel.moveAudio, MyModel.moveAudio)
        }

        MyModel.animationIn(binding.cardInstructionFirst, instructionAudio) // animated instruction
        Handler(Looper.getMainLooper()).postDelayed({
            context?.let { MyModel.vibrate(it, 500) } // delay vibration to sync with the animation
        }, 500)

        myViewModel = ViewModelProvider(requireActivity())[MyViewModel::class.java]
        val myModel = myViewModel.myLiveModel.value
        if (myModel != null) {
            val planetList1 = myModel.planet // assign mixed planet list
            planetAdapter = PlanetAdapter(planetList1) // assign the list created to adapter
            val planetList2 = myModel.planet // assign the mixed planet list to another variable
            displayList.addAll(planetList2) // add the list to list
            planetAdapter = PlanetAdapter(displayList) // assign display list to adapter
            binding.planetRecyclerView.layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false) // set up the layout of recycler view
            binding.planetRecyclerView.adapter = planetAdapter // set up binding for each element in the list

            itemTouchHelper = ItemTouchHelper(simpleCallback) // assign callback
            itemTouchHelper.attachToRecyclerView(binding.planetRecyclerView) // allows to move planets
        }

        binding.buttonNextFirst.setOnClickListener {
            if (PlanetAdapter.FirstMission.isCompleted) {
                PlanetAdapter.FirstMission.isCompleted = false // disable it again if user wants to play the game again
            }
            !PlanetAdapter.FirstMission.isCompleted
            navController = findNavController()
            navController.navigate(R.id.action_firstMissionFragment_to_secondMissionFragment) // go to second mission fragment
            if (MyModel.globalAudio) {
                if (correctAudio.isPlaying)  correctAudio.stop()
                buttonAudio.start()
            }
        }

        binding.buttonInstructionFirst.setOnClickListener {
            if (!dismissButton) {
                MyModel.animationOut(binding.cardInstructionFirst)
                Handler(Looper.getMainLooper()).postDelayed({
                    binding.cardInstructionFirst.animation = null
                }, 1000)
                binding.cardInstructionFirst.visibility = View.GONE
                dismissButton = true // clicked once
            }
            if (MyModel.globalAudio) { // if audio is on
                buttonAudio.start()
                backgroundAudio.start()
            }
        }
    }

    // store object of type ItemTouchHelper to allow interaction in the recycler view
    private var simpleCallback = object :  ItemTouchHelper.SimpleCallback(
        ItemTouchHelper.UP.or(
            ItemTouchHelper.DOWN), 0) {
        override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder): Boolean {
            if (!PlanetAdapter.FirstMission.isCompleted) { // if completed prevent reordering
                val startPosition = viewHolder.adapterPosition
                val endPosition = target.adapterPosition
                Collections.swap(displayList, startPosition, endPosition) // swap positions
                recyclerView.adapter?.notifyItemMoved(startPosition, endPosition) // notify adapter
                if (MyModel.globalAudio) {
                    moveAudio.start()
                }
                checkPlanets() // call internal function
            }
            return true
        }
        override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        }
    }

    /**
     * private fun [checkPlanets] is used to check if they match the sorted list.
     */
    private fun checkPlanets() {
        myViewModel = ViewModelProvider(requireActivity())[MyViewModel::class.java]
        val myModel = myViewModel.myLiveModel.value
        for (i in 0..8) { // from 0 to 8
            if (myModel != null) {
                if (myModel.sortedPlanets[i].name == displayList[i].name) { // if element position matches the sorted element position
                    if (PlanetAdapter.FirstMission.movePlanetAllow) {
                        PlanetAdapter.FirstMission.movePlanetAllow = false // prevent user moving planets if list has been sorted
                    }
                }
                else {
                    PlanetAdapter.FirstMission.movePlanetAllow = true
                    break
                }
            }
        }
        if (!PlanetAdapter.FirstMission.movePlanetAllow) {
            PlanetAdapter.FirstMission.isCompleted = true
            Handler(Looper.getMainLooper()).postDelayed({
                binding.cardMessageFirst.isVisible = true // show delayed message at the completion
                if (MyModel.globalAudio) {
                    backgroundAudio.stop()
                    correctAudio.start()
                }
            }, 1000)
        }
    }

    /**
     * function to pause the audio on stop
     */
    override fun onStop() {
        super.onStop()
        if (MyModel.globalAudio) {
            backgroundAudio.pause()
        }
    }

    /**
     * function to resume the audio on resume
     */
    override fun onResume() {
        super.onResume()
        if (MyModel.globalAudio) {
            backgroundAudio.start()
        }
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment FirstMissionFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            FirstMissionFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}