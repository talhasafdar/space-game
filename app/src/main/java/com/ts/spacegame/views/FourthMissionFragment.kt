package com.ts.spacegame.views

import android.content.Context
import android.content.res.Configuration
import android.graphics.Color
import android.media.MediaPlayer
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.Editable
import android.text.TextWatcher
import android.view.KeyEvent
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AlphaAnimation
import android.view.animation.TranslateAnimation
import android.view.inputmethod.InputMethodManager
import androidx.core.view.isVisible
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.ts.spacegame.R
import com.ts.spacegame.databinding.FragmentFourthMissionBinding
import com.ts.spacegame.models.MyModel

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [FourthMissionFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class FourthMissionFragment : Fragment() {
    private var param1: String? = null
    private var param2: String? = null
    private lateinit var binding: FragmentFourthMissionBinding
    private lateinit var navController: NavController
    private var landButton: Boolean = false
    private var challengeAudio: MediaPlayer = MediaPlayer() // audio
    private var buttonAudio: MediaPlayer = MediaPlayer() // audio
    private var correctAudio: MediaPlayer = MediaPlayer() // audio
    private var wrongAudio: MediaPlayer = MediaPlayer() // audio
    private var spaceshipLand: MediaPlayer = MediaPlayer() // audio

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentFourthMissionBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // for audio
        if (MyModel.globalAudio) {
            challengeAudio = MediaPlayer.create(requireActivity(), R.raw.instruction1)
            challengeAudio.setVolume(MyModel.challengeAudio, MyModel.challengeAudio)
            buttonAudio = MediaPlayer.create(requireActivity(), R.raw.button1)
            buttonAudio.setVolume(MyModel.buttonVolume, MyModel.buttonVolume)
            correctAudio = MediaPlayer.create(requireActivity(), R.raw.success2)
            correctAudio.setVolume(MyModel.correctAudio, MyModel.correctAudio)
            wrongAudio = MediaPlayer.create(requireActivity(), R.raw.wrong1)
            wrongAudio.setVolume(MyModel.wrongAudio, MyModel.wrongAudio)
            spaceshipLand = MediaPlayer.create(requireActivity(), R.raw.spaceship_landing2)
            spaceshipLand.setVolume(MyModel.spaceshipLandAudio, MyModel.spaceshipLandAudio)
        }

        binding.cardChallnegeFourth.isVisible = false
        animateCardIn()
        Handler(Looper.getMainLooper()).postDelayed({
            context?.let { MyModel.vibrate(it, 1000) }
        }, 500)

        binding.astrounatLanding.isVisible = false
        binding.buttonContinueFourth.isVisible = false
        binding.buttonContinueFourth.isEnabled = false
        binding.gravityInput.setRawInputType(Configuration.KEYBOARD_12KEY)
        binding.buttonLandFourth.isEnabled = false
        binding.buttonLandFourth.alpha = 0.5f
        binding.gravityInput.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s.toString().trim().isEmpty()) {
                    binding.buttonLandFourth.isEnabled = false
                    binding.buttonLandFourth.alpha = 0.5f
                } else {
                    binding.gravityInput.setTextColor(Color.BLACK)
                    binding.buttonLandFourth.isEnabled = true
                    binding.buttonLandFourth.alpha = 1.0f
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                binding.buttonLandFourth.isEnabled = false
                binding.buttonLandFourth.alpha = 0.5f
            }

            override fun afterTextChanged(s: Editable?) {
            }
        })

        binding.buttonLandFourth.setOnClickListener {
            if (!landButton) {
                if (binding.buttonLandFourth.isEnabled) {
                    if (binding.gravityInput.text.toString() == "9.807" ||
                        binding.gravityInput.text.toString() == "9.8" ||
                        binding.gravityInput.text.toString() == "9.81" ||
                        binding.gravityInput.text.toString() == "9,807" ||
                        binding.gravityInput.text.toString() == "9,8" ||
                        binding.gravityInput.text.toString() == "9,81") { // if it matches one of these answers
                        binding.gravityInput.setTextColor(Color.GREEN)
                        binding.gravityInput.isFocusable = false
                        hideKeyboard()
                        animateCorrect()
                        landButton = true // landed once
                        if (MyModel.globalAudio) {
                            buttonAudio.start()
                            correctAudio.start()
                        }
                    } else {
                        binding.gravityInput.setTextColor(Color.RED)
                        if (MyModel.globalAudio) {
                            if (buttonAudio.isPlaying) {
                                buttonAudio.seekTo(0)
                            }
                            if (wrongAudio.isPlaying) {
                                wrongAudio.seekTo(0)
                            }
                            buttonAudio.start()
                            wrongAudio.start()
                        }
                    }
                }
            }
        }

        binding.buttonContinueFourth.setOnClickListener {
            navController = findNavController()
            navController.navigate(R.id.action_fourthMissionFragment_to_endFragment) // go to end fragment
            if (MyModel.globalAudio) {
                buttonAudio.start()
            }
        }

        binding.gravityInput.setOnClickListener {
            checkInput() // call internal function
        }
    }

    /**
     * private fun [checkInput] to check if user pressed enter button on the soft keyboard
     * then hide the keyboard.
     */
    private fun checkInput() {
        binding.gravityInput.setOnKeyListener(View.OnKeyListener { _, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_ENTER && event.action == KeyEvent.ACTION_UP) {
                hideKeyboard() // call internal function
                return@OnKeyListener true
            }
            false
        })
    }

    /**
     * private fun [hideKeyboard] is used to hide the soft keyboard
     */
    private fun hideKeyboard() {
        val imm = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(requireView().windowToken, 0) // hide keyboard
    }

    /**
     * private fun [animateCardIn] is used for animating the card view for the fourth mission.
     * it makes the view visible using alpha transition
     */
    private fun animateCardIn() {
        val animaAlphaCard = AlphaAnimation(0.0f, 1.0f)
        animaAlphaCard.duration = 1000
        animaAlphaCard.startOffset = 1000
        animaAlphaCard.fillAfter = true
        binding.cardChallnegeFourth.isVisible = true
        binding.cardChallnegeFourth.startAnimation(animaAlphaCard)
        if (MyModel.globalAudio) { // handler is inside the statement
            Handler(Looper.getMainLooper()).postDelayed({
                if (MyModel.globalAudio) {
                    challengeAudio.start()
                }
            }, 1000)
        }
    }

    /**
     *private fun [animateCorrect] is used to animate hte card view
     * it makes the view invisible using alpha transition
     */
    private fun animateCorrect() {
        val animInstructionCard = AlphaAnimation(1.0f, 0.0f) // for card view
        animInstructionCard.duration = 1000
        animInstructionCard.startOffset = 500
        animInstructionCard.fillAfter = true
        binding.cardChallnegeFourth.startAnimation(animInstructionCard)
        val animCapsule = TranslateAnimation(0f, MyModel.widthWindow - MyModel.widthWindow - 200f, 0f, MyModel.heightWindow - 800f) // capsule
        animCapsule.duration = 2000
        animCapsule.startOffset = 2500
        animCapsule.fillAfter = true
        animCapsule.repeatCount = 0
        binding.landingSpaceship.startAnimation(animCapsule)
        Handler(Looper.getMainLooper()).postDelayed({
            if (MyModel.globalAudio) {
                spaceshipLand.start()
            }
        }, 2500)

        val animAstronaut = AlphaAnimation(0.0f, 1.0f) // astronaut show
        animAstronaut.duration = 2000
        animAstronaut.startOffset = 4000
        animAstronaut.fillAfter = true
        binding.astrounatLanding.isVisible = true
        binding.astrounatLanding.startAnimation(animAstronaut)

        val animContinue = AlphaAnimation(0.0f, 1.0f) // continue button show
        animContinue.duration = 2000
        animContinue.startOffset = 5500
        animContinue.fillAfter = true
        Handler(Looper.getMainLooper()).postDelayed({
            binding.buttonContinueFourth.isVisible = true
        }, 2000)
        binding.buttonContinueFourth.startAnimation(animContinue)
        Handler(Looper.getMainLooper()).postDelayed({
            binding.buttonContinueFourth.isEnabled = true
        }, 2500)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment FourthMissionFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            FourthMissionFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}