package com.ts.spacegame.models

import android.graphics.Canvas
import android.graphics.Rect
import android.graphics.drawable.Drawable

/**
 * The abstract class [GameObject] is extended by other classes to create game objects.
 * it sets up the width and height of the object and the rectangle for the collider.
 * it updates the position of the object on the view.
 */
open class GameObject(var x: Int, var y: Int, var dx: Int, var dy: Int, var image: Drawable) {
    open var width: Int = 150 // global width size
    open var height: Int = 150 // global height size
    open var rectObject: Rect = Rect() // initialise rectangle for the collision detection

    /**
     * open fun [move], overrideable function to update the position of the object on the view.
     */
    open fun move(canvas: Canvas) {
        x += dx
        y += dy

        if (x > (canvas.width - width) || x < 0) {
            dx = -dx
        }
        if (y > (canvas.height - height) || y < 0) {
            dy = -dy
        }
        image.setBounds(x, y, x + width, y + width)
        updateRectangle() // call internal function
        image.draw(canvas)
    }

    /**
     * private fun [updateRectangle] is used to update the position
     * of the rectangle for the collider.
     */
    private fun updateRectangle() {
        rectObject = Rect(x, y, x + width, y + height)
    }
}