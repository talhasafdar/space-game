package com.ts.spacegame.views
import android.annotation.SuppressLint
import android.media.MediaPlayer
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.RotateAnimation
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.ts.spacegame.R
import com.ts.spacegame.databinding.FragmentHomeBinding
import com.ts.spacegame.models.MyModel

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [HomeFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class HomeFragment : Fragment() {
    private var param1: String? = null
    private var param2: String? = null
    private lateinit var binding: FragmentHomeBinding
    private lateinit var navController: NavController
    private var isMute: Boolean = false // toggle audio
    private var backgroundAudio: MediaPlayer = MediaPlayer() // audio

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // for audio
        backgroundAudio = MediaPlayer.create(requireActivity(), R.raw.background1)
        backgroundAudio.setVolume(MyModel.backgroundVolume, MyModel.backgroundVolume)
        backgroundAudio.isLooping = true

        if (MyModel.globalAudio) {
            binding.volumeToggle.setImageResource(R.drawable.volume_on)
        }
        if (!MyModel.globalAudio) {
            binding.volumeToggle.setImageResource(R.drawable.volume_off)
        }

        binding.playButton.setOnClickListener {
            if (MyModel.globalAudio) {
                if (backgroundAudio.isPlaying) backgroundAudio.stop()
            }
            navController = findNavController()
            navController.navigate(R.id.action_homeFragment_to_firstMissionFragment) // go to first mission fragment
        }

        binding.aboutButton.setOnClickListener {
            navController = findNavController()
            navController.navigate(R.id.action_homeFragment_to_aboutMeFragment) // go to about me fragment
        }

        binding.volumeToggle.setOnClickListener {
            if (!isMute) {
                binding.volumeToggle.setImageResource(R.drawable.volume_off)
                isMute = true
                MyModel.globalAudio = false
                if (backgroundAudio.isPlaying) {
                    backgroundAudio.pause()
                    backgroundAudio.seekTo(0)
                }
            } else {
                binding.volumeToggle.setImageResource(R.drawable.volume_on)
                isMute = false
                MyModel.globalAudio = true
                if (!backgroundAudio.isPlaying) {
                    backgroundAudio.start()
                }
            }
        }

        val animAsteroid = RotateAnimation(0f, 359f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f)
        animAsteroid.duration = 15000
        animAsteroid.fillAfter = true
        animAsteroid.repeatCount = Animation.INFINITE
        animAsteroid.setInterpolator(context, android.R.anim.linear_interpolator)
        binding.asteroidStart1.startAnimation(animAsteroid)
        binding.asteroidStart2.startAnimation(animAsteroid)
    }

    /**
     * function to pause the audio on stop
     */
    override fun onStop() {
        super.onStop()
        if (MyModel.globalAudio) {
            backgroundAudio.pause()
        }
    }

    /**
     * function to resume the audio on resume
     */
    override fun onResume() {
        super.onResume()
        if (MyModel.globalAudio) {
            backgroundAudio.start()
        }
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment HomeFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            HomeFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}