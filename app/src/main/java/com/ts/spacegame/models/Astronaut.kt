package com.ts.spacegame.models

import android.graphics.Canvas
import android.graphics.Rect
import android.graphics.drawable.Drawable

/**
 * The class [Astronaut] is used to generate asteroids objects.
 * it extends the class [GameObject].
 */
@Suppress("KotlinConstantConditions")
class Astronaut(x: Int, y: Int, dx: Int, dy: Int, image: Drawable) : GameObject(x, y, dx, dy, image) {
    var px: Int = MyModel.widthWindow / 2 - 100 // center the pivot when dragging it
    var py: Int = MyModel.heightWindow - 1300 // set the pivot point lower to make the object visible when dragging it
    private var increaseSize: Int = 50 // increase size
    var rectPlayer: Rect = Rect(0, 0, width + increaseSize, height + increaseSize)

    /**
     * override fun [move] is used to update the position
     * of the object on the view.
     */
    override fun move(canvas: Canvas) {
        x += when {
            px > x -> 20
            px == x -> 0
            px < x -> -20
            else -> {
                x
            }
        }

        y += when {
            py > y -> 20
            py == y -> 0
            py < y -> -20
            else -> {
                y
            }
        }

        if (px > x) {
            x += 20
        } else if (px < x) {
            x -= 20
        }

        if (py > y) {
            y += 20
        } else if (py < y) {
            y -= 20
        }

        image.setBounds(x, y, x + width + increaseSize, y + height + increaseSize)
        updateRectangle() // call internal function
        image.draw(canvas)
    }

    /**
     * private fun [updateRectangle] is used to update the position
     * of the rectangle for the collider.
     */
    private fun updateRectangle() {
        rectPlayer = Rect(x, y, x + width + increaseSize, y + height + increaseSize)
    }
}