package com.ts.spacegame.viewModels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ts.spacegame.models.MyModel

/**
 * The class [MyViewModel] is used to manage the MyModel class.
 * it extends the class [ViewModel].
 */
class MyViewModel : ViewModel() {
    val myLiveModel = MutableLiveData<MyModel>()

    init {
        myLiveModel.value = MyModel()
    }
}