package com.ts.spacegame.views

import android.annotation.SuppressLint
import android.media.MediaPlayer
import android.os.*
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.cardview.widget.CardView
import androidx.core.view.isVisible
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.ts.spacegame.R
import com.ts.spacegame.viewModels.SecondMission
import com.ts.spacegame.databinding.FragmentSecondMissionBinding
import com.ts.spacegame.models.MyModel
import java.util.*

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [SecondMissionFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class SecondMissionFragment : Fragment() {
    private var param1: String? = null
    private var param2: String? = null
    private lateinit var binding: FragmentSecondMissionBinding
    private lateinit var navController: NavController
    private lateinit var cardMessage: CardView
    private var dismissButton: Boolean = false
    private var instructionAudio: MediaPlayer = MediaPlayer() // audio
    private var buttonAudio: MediaPlayer = MediaPlayer() // audio
    private var correctAudio: MediaPlayer = MediaPlayer() // audio
    private var retryAudio: MediaPlayer = MediaPlayer() // audio
    private var enterAudio: MediaPlayer = MediaPlayer() // audio
    private var backgroundAudio: MediaPlayer = MediaPlayer() // audio
    private var playOnResume: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentSecondMissionBinding.inflate(inflater, container, false)
        return binding.root
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.buttonRetrySecond.isVisible = false
        cardMessage = binding.cardMessageSecond

        // for audio
        if (MyModel.globalAudio) {
            backgroundAudio = MediaPlayer.create(requireActivity(), R.raw.background_mission_2)
            backgroundAudio.setVolume(MyModel.backgroundSecondVolume, MyModel.backgroundSecondVolume)
            backgroundAudio.isLooping = true
            instructionAudio = MediaPlayer.create(requireActivity(), R.raw.instruction1)
            instructionAudio.setVolume(MyModel.animationInVolume, MyModel.animationInVolume)
            buttonAudio = MediaPlayer.create(requireActivity(), R.raw.button1)
            buttonAudio.setVolume(MyModel.buttonVolume, MyModel.buttonVolume)
            correctAudio = MediaPlayer.create(requireActivity(), R.raw.correct1)
            correctAudio.setVolume(MyModel.correctAudio, MyModel.correctAudio)
            retryAudio = MediaPlayer.create(requireActivity(), R.raw.retry2)
            retryAudio.setVolume(MyModel.retryAudio, MyModel.retryAudio)
            enterAudio = MediaPlayer.create(requireActivity(), R.raw.enter1)
            enterAudio.setVolume(MyModel.enterAudio, MyModel.enterAudio)
        }

        // animation
        MyModel.animationIn(binding.cardInstructionSecond, instructionAudio)
        Handler(Looper.getMainLooper()).postDelayed({
            context?.let { MyModel.vibrate(it, 500) } // delay vibration to sync with the animation
        }, 500)

        val checkMissionEnded = SecondMissionUpdater() // initialise inner class to check if the mission has been completed
        checkMissionEnded.run() // run the function
        binding.buttonNextSecond.setOnClickListener {
            if (SecondMission.isMissionEnded) SecondMission.isMissionEnded = false // disable if want to play again
            if (SecondMission.isMissionCompleted) SecondMission.isMissionCompleted = false // disable if want to play again
            if (SecondMission.isStarted) SecondMission.isStarted = false // disable if want to play again
            navController = findNavController()
            navController.navigate(R.id.action_secondMissionFragment_to_thirdMissionFragment) // go to third mission fragment
            if (MyModel.globalAudio) {
                buttonAudio.start()
            }
        }

        binding.buttonRetrySecond.setOnClickListener {
            binding.buttonRetrySecond.isVisible = false
            SecondMission.isPaused = false
            if (MyModel.globalAudio) {
                if (retryAudio.isPlaying) { // to ensure it is reset before retrying
                    retryAudio.pause()
                    retryAudio.seekTo(0)
                }
                backgroundAudio.start()
            }
        }

        binding.buttonInstructionSecond.setOnClickListener {
            if (!dismissButton) {
                MyModel.animationOut(binding.cardInstructionSecond)
                Handler(Looper.getMainLooper()).postDelayed({
                    binding.cardInstructionSecond.animation = null
                }, 1000)
                binding.cardInstructionSecond.visibility = View.GONE
                dismissButton = true // clicked once
                SecondMission.isStarted = true
                backgroundAudio.start()
            }
            if (MyModel.globalAudio) {
                buttonAudio.start()
            }
        }
    }

    /**
     * inner class [SecondMissionUpdater] is used to observe and respond accordingly based on the boolean variables.
     * it checks if mission is completed or the user needs to retry.
     * it uses a thread to ensure a smooth experience.
     */
    inner class SecondMissionUpdater : Runnable {
        override fun run() {
            Thread { // add while loop within the thread
                while (!SecondMission.isMissionEnded) { // if mission is not ended
                    if (SecondMission.isMissionCompleted) { // if completed
                        SecondMission.isMissionEnded = true // set mission ended
                        if (MyModel.globalAudio) {
                            if (backgroundAudio.isPlaying) {
                                backgroundAudio.stop()
                            }
                            enterAudio.start()
                        }
                        Handler(Looper.getMainLooper()).postDelayed({
                            binding.cardMessageSecond.isVisible = true
                            if (MyModel.globalAudio) {
                                correctAudio.start()
                            }
                        }, 1000)
                    } else if (SecondMission.retry) { // if users needs to retry
                        Handler(Looper.getMainLooper()).postDelayed({
                            binding.buttonRetrySecond.isVisible = true
                        }, 1)
                        SecondMission.retry = false
                        if (MyModel.globalAudio) {
                            backgroundAudio.pause()
                            backgroundAudio.seekTo(0)
                            retryAudio.start()
                        }
                    }
                }
            }.start()
        }
    }

    /**
     * function to pause the audio on stop
     */
    override fun onStop() {
        super.onStop()
        if (MyModel.globalAudio) {
            if (backgroundAudio.isPlaying) {
                backgroundAudio.pause()
                playOnResume = true
            }
        }
    }

    /**
     * function to resume the audio on resume
     */
    override fun onResume() {
        super.onResume()
        if (MyModel.globalAudio) {
            if (playOnResume) {
                backgroundAudio.start()
                playOnResume = false
            }
        }
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment SecondMissionFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            SecondMissionFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}