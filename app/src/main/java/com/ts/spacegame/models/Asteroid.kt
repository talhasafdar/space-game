package com.ts.spacegame.models

import android.graphics.Canvas
import android.graphics.Rect
import android.graphics.drawable.Drawable

/**
 * The class [Asteroid] is used to generate asteroids objects.
 * it extends the class [GameObject].
 */
class Asteroid(x: Int, y: Int, dx: Int, dy: Int, image: Drawable) : GameObject(x, y, dx, dy, image) {

    /**
     * override fun [move] is used to update the position
     * of the object on the view.
     */
    override fun move(canvas: Canvas) {
        x += dx
        y += dy
        if (x > (canvas.width - width) || x < 0) {
            dx = -dx
        }
        if (y > (canvas.height - height) || y < 0) {
            dy = -dy
        }
        image.setBounds(x, y, x + width, y + height)
        updateRectangle() // call internal function
        image.draw(canvas)
    }

    /**
     * private fun [updateRectangle] is used to update the position
     * of the rectangle for the collider.
     */
    private fun updateRectangle() {
        rectObject = Rect(x, y, x + width, y + height)
    }
}