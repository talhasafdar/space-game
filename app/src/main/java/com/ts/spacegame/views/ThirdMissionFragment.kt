package com.ts.spacegame.views

import android.content.Context
import android.hardware.*
import android.media.MediaPlayer
import android.os.*
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.doOnLayout
import androidx.core.view.isVisible
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.ts.spacegame.R
import com.ts.spacegame.viewModels.ThirdMission
import com.ts.spacegame.databinding.FragmentThirdMissionBinding
import com.ts.spacegame.models.MyModel

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [ThirdMissionFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ThirdMissionFragment : Fragment(), SensorEventListener {
    private var param1: String? = null
    private var param2: String? = null
    private lateinit var binding: FragmentThirdMissionBinding
    private lateinit var navController: NavController
    private lateinit var sensorManager: SensorManager
    private lateinit var accelerometer: Sensor
    private var checkMissionEnded: FourthMissionUpdater = FourthMissionUpdater()
    private var dismissButton: Boolean = false
    private var instructionAudio: MediaPlayer = MediaPlayer() // audio
    private var buttonAudio: MediaPlayer = MediaPlayer() // audio
    private var correctAudio: MediaPlayer = MediaPlayer() // audio
    private var retryAudio: MediaPlayer = MediaPlayer() // audio
    private var spaceshipAudio: MediaPlayer = MediaPlayer() // audio
    private var spaceshipEnterAudio: MediaPlayer = MediaPlayer() // audio
    private var playOnResume: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentThirdMissionBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.buttonRetryThird.isVisible = false

        // for audio
        if (MyModel.globalAudio) {
            instructionAudio = MediaPlayer.create(requireActivity(), R.raw.instruction1)
            instructionAudio.setVolume(MyModel.animationInVolume, MyModel.animationInVolume)
            buttonAudio = MediaPlayer.create(requireActivity(), R.raw.button1)
            buttonAudio.setVolume(MyModel.buttonVolume, MyModel.buttonVolume)
            correctAudio = MediaPlayer.create(requireActivity(), R.raw.correct1)
            correctAudio.setVolume(MyModel.correctAudio, MyModel.correctAudio)
            retryAudio = MediaPlayer.create(requireActivity(), R.raw.retry2)
            retryAudio.setVolume(MyModel.retryAudio, MyModel.retryAudio)
            spaceshipAudio = MediaPlayer.create(requireActivity(), R.raw.spaceship2)
            spaceshipAudio.setVolume(MyModel.spaceshipAudio, MyModel.spaceshipAudio)
            spaceshipAudio.isLooping = true
            spaceshipEnterAudio = MediaPlayer.create(requireActivity(), R.raw.spaceship_enter1)
            spaceshipEnterAudio.setVolume(
                MyModel.spaceshipEnterVolume,
                MyModel.spaceshipEnterVolume
            )
        }

        // animation
        MyModel.animationIn(binding.cardInstructionThird, instructionAudio)
        Handler(Looper.getMainLooper()).postDelayed({
            context?.let { MyModel.vibrate(it, 500) } // delay vibration to sync with the animation
        }, 500)

        // hide message
        binding.cardMessageThird.isVisible = false

        binding.buttonNextThird.setOnClickListener {
            if (ThirdMission.isMissionEnded) ThirdMission.isMissionEnded = false // disable if want to play again
            if (ThirdMission.isMissionCompleted) ThirdMission.isMissionCompleted = false // disable if want to play again
            if (ThirdMission.isStarted) ThirdMission.isStarted = false // disable if want to play again
            navController = findNavController()
            navController.navigate(R.id.action_thirdMissionFragment_to_fourthMissionFragment) // go to fourth mission fragment
            if (MyModel.globalAudio) {
                buttonAudio.start()
            }
        }


        binding.buttonRetryThird.setOnClickListener {
            binding.buttonRetryThird.isVisible = false
            ThirdMission.isPaused = false
            if (MyModel.globalAudio) {
                if (retryAudio.isPlaying) { // to ensure it is reset before retrying
                    retryAudio.pause()
                    retryAudio.seekTo(0)
                }
                spaceshipAudio.seekTo(0)
                spaceshipAudio.start()
            }
        }

        sensorManager = requireActivity().getSystemService(Context.SENSOR_SERVICE) as SensorManager

        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)
        sensorManager.registerListener(
            this,
            accelerometer,
            SensorManager.SENSOR_STATUS_ACCURACY_LOW
        )

        checkMissionEnded.run()

        binding.buttonInstructionThird.setOnClickListener {
            if (!dismissButton) {
                MyModel.animationOut(binding.cardInstructionThird)
                Handler(Looper.getMainLooper()).postDelayed({
                    binding.cardInstructionThird.animation = null
                }, 2000)
                binding.cardInstructionThird.visibility = View.GONE
                dismissButton = true // clicked once
                ThirdMission.isStarted = true
                if (!ThirdMission.restarted) ThirdMission.restarted = true
            }
            if (MyModel.globalAudio) {
                buttonAudio.start()
                spaceshipAudio.start()
            }
        }
    }

    /**
     * override fun [onSensorChanged] is sued to obtain the XYZ values of the device.
     */
    override fun onSensorChanged(event: SensorEvent?) {
        if (event == null)
            return
        if (event.sensor.type == Sensor.TYPE_ACCELEROMETER) {
            val x = event.values[0]

            var w = 0

            binding.spaceshipView.doOnLayout {
                w = it.measuredWidth
            }
            binding.spaceshipView.myX = w / 2f - x * 90f
            binding.spaceshipView.invalidate()
        }
    }

    override fun onAccuracyChanged(event: Sensor?, p1: Int) {
    }

    /**
     * inner class [FourthMissionUpdater] is used to observe and respond accordingly based on the boolean variables.
     * it checks if mission is completed or the user needs to retry.
     * it uses a thread to ensure a smooth experience.
     */
    inner class FourthMissionUpdater : Runnable {
        override fun run() {
            Thread { // add while loop within the thread
                while (!ThirdMission.isMissionEnded) {
                    if (ThirdMission.isMissionCompleted) {
                        ThirdMission.isMissionEnded = true
                        if (MyModel.globalAudio) {
                            spaceshipEnterAudio.start()
                        }
                        Handler(Looper.getMainLooper()).postDelayed({
                            binding.cardMessageThird.isVisible = true
                            if (MyModel.globalAudio) {
                                correctAudio.start()
                                if (spaceshipAudio.isPlaying) spaceshipAudio.stop()
                            }
                        }, 2000)
                    } else if (ThirdMission.retry) {
                        Handler(Looper.getMainLooper()).postDelayed({
                            binding.buttonRetryThird.isVisible = true
                        }, 1)
                        ThirdMission.retry = false
                        if (MyModel.globalAudio) {
                            retryAudio.start()
                            spaceshipAudio.pause()
                        }
                    }
                }
            }.start()
        }
    }

    /**
     * function to pause the audio on stop
     */
    override fun onStop() {
        super.onStop()
        if (MyModel.globalAudio) {
            if (spaceshipAudio.isPlaying) {
                spaceshipAudio.pause()
                playOnResume = true
            }
        }
    }

    /**
     * function to resume the audio on resume
     */
    override fun onResume() {
        super.onResume()
        if (MyModel.globalAudio) {
            if (playOnResume) {
                spaceshipAudio.start()
                playOnResume = false
            }
        }
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment ThirdMissionFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            ThirdMissionFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}