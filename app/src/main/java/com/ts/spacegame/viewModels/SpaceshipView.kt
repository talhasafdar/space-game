package com.ts.spacegame.viewModels

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View
import com.ts.spacegame.R
import com.ts.spacegame.models.Asteroid
import com.ts.spacegame.models.MyModel
import com.ts.spacegame.models.Spaceship

/**
 * The class [SpaceshipView] is used to generate a view for the third mission.
 * it extends the clas [View].
 * it sets objects from other classes alongside their colliders.
 * it checks for collision between the player and the other objects.
 * it includes a singleton for relative variables.
 */
@SuppressLint("UseCompatLoadingForDrawables")
class SpaceshipView(context: Context?, attrs: AttributeSet?) : View(context, attrs) {
    var myX: Float = 0f // to obtain the x coordinate for the player
    private var restarted: Boolean = false // to check if the game is restarted
    private val spaceshipImage = context!!.resources.getDrawable(R.drawable.spaceship_moving, null) // obtain the image for the spaceship
    private var spaceship = Spaceship(0,0, 0, 0, spaceshipImage) // default position for the player
    private var moveSpaceship: Int = MyModel.heightWindow - MyModel.heightWindow - spaceship.height - 100 // move player vertically
    private var speed: Int = 20 // set up speed for the player
    private var currentX: Int = 0 // check current x coordinate of the player
    private var startPoint: Int = MyModel.heightWindow - 500 // set start point for the player
    private var rectSpaceship = Rect(spaceship.rectPlayer) // set up collider for the player
    private var colliderAsteroid1 = Rect() // set up collider for asteroid
    private var colliderAsteroid2 = Rect() // set up collider for asteroid
    private var colliderAsteroid3 = Rect() // set up collider for asteroid
    private var colliderAsteroid4 = Rect() // set up collider for asteroid
    private var colliderAsteroid5 = Rect() // set up collider for asteroid
    private var colliderAsteroid6 = Rect() // set up collider for asteroid
    private var colliderAsteroid7 = Rect() // set up collider for asteroid
    private var colliderAsteroid8 = Rect() // set up collider for asteroid
    private var colliderAsteroid9 = Rect() // set up collider for asteroid
    private var colliderAsteroid10 = Rect() // set up collider for asteroid
    private var colliderAsteroid11 = Rect() // set up collider for asteroid
    private var colliderAsteroid12 = Rect() // set up collider for asteroid
    private var colliderAsteroid13 = Rect() // set up collider for asteroid
    private var startedMoving: Boolean = true // check if player started moving
    private var asteroids = ArrayList<Asteroid>() // create an arraylist of asteroids

    init {
        if (context != null) {
            // add each asteroid object in the arraylist
            asteroids.add(Asteroid(MyModel.widthWindow - 1200, MyModel.heightWindow - 2100, 0, 0, context.resources.getDrawable(R.drawable.asteroid, null)))
            asteroids.add(Asteroid(MyModel.widthWindow - 1050, MyModel.heightWindow - 1200, 0, 0, context.resources.getDrawable(R.drawable.asteroid, null)))
            asteroids.add(Asteroid(MyModel.widthWindow - 1100, MyModel.heightWindow - 900, 0, 0, context.resources.getDrawable(R.drawable.meteorite, null)))
            asteroids.add(Asteroid(MyModel.widthWindow - 500, MyModel.heightWindow - 2300, 0, 0, context.resources.getDrawable(R.drawable.asteroid2, null)))
            asteroids.add(Asteroid(MyModel.widthWindow - 800, MyModel.heightWindow - 1800, 0, 0, context.resources.getDrawable(R.drawable.asteroids, null)))
            asteroids.add(Asteroid(MyModel.widthWindow - 400, MyModel.heightWindow - 1800, 0, 0, context.resources.getDrawable(R.drawable.asteroids, null)))
            asteroids.add(Asteroid(MyModel.widthWindow - 1400, MyModel.heightWindow - 1800, 0, 0, context.resources.getDrawable(R.drawable.asteroid6, null)))
            asteroids.add(Asteroid(MyModel.widthWindow - 400, MyModel.heightWindow - 1500, 0, 0, context.resources.getDrawable(R.drawable.meteorite, null)))
            asteroids.add(Asteroid(MyModel.widthWindow - 1300, MyModel.heightWindow - 1400, 0, 0, context.resources.getDrawable(R.drawable.asteroid4, null)))
            asteroids.add(Asteroid(MyModel.widthWindow - 600, MyModel.heightWindow - 2000, 0, 0, context.resources.getDrawable(R.drawable.asteroid4, null)))
            asteroids.add(Asteroid(MyModel.widthWindow / 2, MyModel.heightWindow - 1100, 0, 0, context.resources.getDrawable(R.drawable.asteroid2, null)))
            asteroids.add(Asteroid(MyModel.widthWindow - 350, MyModel.heightWindow - 700, 0, 0, context.resources.getDrawable(R.drawable.asteroid5, null)))
            asteroids.add(Asteroid(MyModel.widthWindow - 1000, MyModel.heightWindow - 2400, 0, 0, context.resources.getDrawable(R.drawable.asteroid5, null)))
        }
    }

    /**
     * override fun [onDrawForeground] is used to draw the contents on the view.
     * it checks if the game is in not pause mode and is started.
     * it checks if the game is restarted.
     * it updates the collider for the player and other objects.
     * it checks if player hits the edge of the screen and keep the player inside the viewable area.
     * it checks if it collides with other objects and then it notifies the view
     * it restores the position if collided
     * it checks if the player reached the top of the view and then notify the view
     * it updates the movement of the player by incrementing by the speed value.
     */
    override fun onDrawForeground(canvas: Canvas?) {
        super.onDrawForeground(canvas)
        if (!ThirdMission.isPaused && ThirdMission.isStarted) {
            if (canvas != null) {
                if (ThirdMission.restarted) { // if replaying again
                    moveSpaceship = MyModel.heightWindow - MyModel.heightWindow - spaceship.height - 100 // reset the position
                    startPoint = MyModel.heightWindow - 500 // reset the start point
                    ThirdMission.restarted = false
                }
                rectSpaceship = Rect(spaceship.rectPlayer) // update collider potion relative to the player
                for(asteroid in asteroids) { // for each object in the list update the position of the collider relatively
                    asteroid.move(canvas)
                    colliderAsteroid1 = Rect(asteroids[0].rectObject) // collider object
                    colliderAsteroid2 = Rect(asteroids[1].rectObject) // collider object
                    colliderAsteroid3 = Rect(asteroids[2].rectObject) // collider object
                    colliderAsteroid4 = Rect(asteroids[3].rectObject) // collider object
                    colliderAsteroid5 = Rect(asteroids[4].rectObject) // collider object
                    colliderAsteroid6 = Rect(asteroids[5].rectObject) // collider object
                    colliderAsteroid7 = Rect(asteroids[6].rectObject) // collider object
                    colliderAsteroid8 = Rect(asteroids[7].rectObject) // collider object
                    colliderAsteroid9 = Rect(asteroids[8].rectObject) // collider object
                    colliderAsteroid10 = Rect(asteroids[9].rectObject) // collider object
                    colliderAsteroid11 = Rect(asteroids[10].rectObject) // collider object
                    colliderAsteroid12 = Rect(asteroids[11].rectObject) // collider object
                    colliderAsteroid13 = Rect(asteroids[12].rectObject) // collider object
                }

                if (myX > 150 && myX < canvas.width-spaceship.width) { // check if player stays inside the edges
                    spaceship = Spaceship(myX.toInt() - 110, startPoint-moveSpaceship, 0, 0, spaceshipImage)
                    currentX = myX.toInt() // update the x coordinate value
                    spaceship.move(canvas)
                } else { // update the x coordinate value to 0 if hit the edge
                    spaceship = Spaceship(currentX - 110, startPoint-moveSpaceship, 0, 0, spaceshipImage)
                    spaceship.move(canvas)
                }

                if (startedMoving) {
                    if (rectSpaceship.intersect(colliderAsteroid1) ||
                        rectSpaceship.intersect(colliderAsteroid2) ||
                        rectSpaceship.intersect(colliderAsteroid3) ||
                        rectSpaceship.intersect(colliderAsteroid4) ||
                        rectSpaceship.intersect(colliderAsteroid5) ||
                        rectSpaceship.intersect(colliderAsteroid6) ||
                        rectSpaceship.intersect(colliderAsteroid7) ||
                        rectSpaceship.intersect(colliderAsteroid8) ||
                        rectSpaceship.intersect(colliderAsteroid9) ||
                        rectSpaceship.intersect(colliderAsteroid10) ||
                        rectSpaceship.intersect(colliderAsteroid11) ||
                        rectSpaceship.intersect(colliderAsteroid12) ||
                        rectSpaceship.intersect(colliderAsteroid13)) { // check if player collided with an object
                        moveSpaceship = MyModel.heightWindow - MyModel.heightWindow - spaceship.height - 100
                        startPoint = MyModel.heightWindow - 500
                        ThirdMission.retry = true // notify the view
                        ThirdMission.isPaused = true // pause the view
                        startedMoving = false
                    }
                } else {
                    startedMoving = true
                }

                // if wins
                if (spaceship.y < canvas.height - height + spaceship.height) {
                    ThirdMission.isMissionCompleted = true // if mission is completed notify the view
                    restarted = true
                }
            }
            moveSpaceship += speed // update the position
        }
    }
}

/**
 * singleton object [ThirdMission] used to monitor and notify the view.
 */
object ThirdMission {
    var restarted: Boolean = false // check if restarted
    var isStarted: Boolean = false // check if mission is started
    var isMissionCompleted: Boolean = false // for completion check
    var isMissionEnded: Boolean = false // for stopping run() in view
    var retry: Boolean = false // check if retry
    var isPaused: Boolean = false // for try again
}