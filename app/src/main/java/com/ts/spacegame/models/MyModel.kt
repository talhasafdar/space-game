package com.ts.spacegame.models

import android.content.Context
import android.media.MediaPlayer
import android.os.*
import android.view.View
import android.view.animation.AlphaAnimation
import com.ts.spacegame.R

/**
 * the class [MyModel] is used to set up two lists of planets
 * one sorted and one shuffled.
 * it contains a companion object to allow different fragments to access the same functions.
 */
class MyModel {
    var planet = ArrayList<PlanetItem>() // shuffled list
    var sortedPlanets = ArrayList<PlanetItem>() // perfect list check

    init {
        planet.add(PlanetItem("Earth", R.drawable.earth))
        planet.add(PlanetItem("Jupiter", R.drawable.jupiter))
        planet.add(PlanetItem("Mars", R.drawable.mars))
        planet.add(PlanetItem("Mercury", R.drawable.mercury))
        planet.add(PlanetItem("Neptune", R.drawable.neptune))
        planet.add(PlanetItem("Pluto", R.drawable.pluto))
        planet.add(PlanetItem("Saturn", R.drawable.saturn))
        planet.add(PlanetItem("Uranus", R.drawable.uranus))
        planet.add(PlanetItem("Venus", R.drawable.venus))

        sortedPlanets.add(PlanetItem("Mercury", R.drawable.mercury))
        sortedPlanets.add(PlanetItem("Venus", R.drawable.venus))
        sortedPlanets.add(PlanetItem("Earth", R.drawable.earth))
        sortedPlanets.add(PlanetItem("Mars", R.drawable.mars))
        sortedPlanets.add(PlanetItem("Jupiter", R.drawable.jupiter))
        sortedPlanets.add(PlanetItem("Saturn", R.drawable.saturn))
        sortedPlanets.add(PlanetItem("Uranus", R.drawable.uranus))
        sortedPlanets.add(PlanetItem("Neptune", R.drawable.neptune))
        sortedPlanets.add(PlanetItem("Pluto", R.drawable.pluto))
    }

    /**
     * companion object [StaticData] allows fragments to use same functions.
     */
    companion object StaticData {

        // screen size
        var heightWindow = 0
        var widthWindow = 0

        // audio global
        var globalAudio: Boolean = true
        var animationInVolume: Float = 0.5f
        var buttonVolume: Float = 0.2f
        var correctAudio: Float = 0.4f
        var backgroundVolume: Float = 1.0f

        // #1
        var moveAudio: Float = 0.5f

        // #2
        var backgroundSecondVolume: Float = 0.3f
        var enterAudio: Float = 0.05f

        // #3
        var spaceshipAudio: Float = 0.5f
        var spaceshipEnterVolume: Float = 0.5f

        // #2, #3
        var retryAudio: Float = 0.3f

        // #4
        var challengeAudio: Float = 0.5f
        var wrongAudio: Float = 0.5f
        var spaceshipLandAudio: Float = 0.5f

        // end
        var backgroundEndAudio: Float = 0.1f
        var finalMessageAudio: Float = 0.1f
        var swipeUpAudio: Float = 0.5f

        /**
         * fun [vibrate] used to emit vibration on the device.
         * it checks the SDK version and executes the appropriate conditional statement.
         */
        fun vibrate(context: Context, delay: Long){
            val vib = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                val vibratorManager =  context.getSystemService(Context.VIBRATOR_MANAGER_SERVICE) as VibratorManager
                vibratorManager.defaultVibrator
            } else {
                @Suppress("DEPRECATION")
                context.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                vib.vibrate(VibrationEffect.createOneShot(delay, VibrationEffect.DEFAULT_AMPLITUDE))
            }else{
                @Suppress("DEPRECATION")
                vib.vibrate(delay)
            }
        }

        /**
         * fun [animationIn] used to show the notification box in the fragments
         */
        fun animationIn(view: View, audio: MediaPlayer) {
            val animAlphaInstruction = AlphaAnimation(0.0f, 1.0f)
            animAlphaInstruction.duration = 500
            animAlphaInstruction.startOffset = 500
            animAlphaInstruction.fillAfter = true
            view.startAnimation(animAlphaInstruction)
            if (globalAudio) {
                Handler(Looper.getMainLooper()).postDelayed({
                    audio.start()
                }, 500)
            }
        }

        /**
         * fun [animationOut] used to hide the notification box in the fragments
         */
        fun animationOut(view: View) {
            val animAlphaInstruction = AlphaAnimation(1.0f, 0.0f)
            animAlphaInstruction.duration = 350
            animAlphaInstruction.fillAfter = true
            view.startAnimation(animAlphaInstruction)
            Handler(Looper.getMainLooper()).postDelayed({
                view.animation = null
            }, 1000)
        }
    }
}