package com.ts.spacegame.viewModels

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.ts.spacegame.databinding.PlanetItemViewBinding
import com.ts.spacegame.models.PlanetItem

/**
 * the class [PlanetAdapter] is used to manage the recycler view.
 * it extends the class [RecyclerView].
 * it servers to display the contents for the recycler view.
 * it has a singleton objects to notify the view about the contents.
 */
@Suppress("JoinDeclarationAndAssignment")
class PlanetAdapter(private var planetList: MutableList<PlanetItem>) :
    RecyclerView.Adapter<PlanetAdapter.ViewHolder>() {

    private var planets: MutableList<PlanetItem> = planetList // list of shuffled planets

    /**
     * override fun [onCreateViewHolder] is used to set up the binding.
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemBinding = PlanetItemViewBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(itemBinding)
    }

    /**
     * override fun [onBindViewHolder] is sued to assign the name and the image for each element in the list.
     */
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val planetItem = planetList[position]
        holder.itemBinding.textItem.text = planetItem.name
        holder.itemBinding.planetItem.setImageResource(planetItem.image)
    }

    /**
     * override fun [getItemCount] returns the size of the list containing the elements.
     */
    override fun getItemCount(): Int {
        return planets.size
    }

    /**
     * inner clas [ViewHolder] is used to assign each value to the local variables.
     * it extends the class [RecyclerView].
     */
    inner class ViewHolder(var itemBinding: PlanetItemViewBinding) : RecyclerView.ViewHolder(itemBinding.root) {
        private var itemImage: ImageView
        private var itemTitle: TextView

        init {
            itemImage = itemBinding.planetItem
            itemTitle = itemBinding.textItem
        }
    }

    /**
     * singleton object [FirstMission] is used to notify the view.
     */
    object FirstMission {
        var isCompleted: Boolean = false // if mission is completed
        var movePlanetAllow = true // for moving the planets in the recycler view
    }
}