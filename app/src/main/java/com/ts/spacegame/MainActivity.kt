package com.ts.spacegame

import android.content.res.Resources
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.*
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.view.WindowCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.WindowInsetsControllerCompat
import com.ts.spacegame.models.MyModel


class MainActivity : AppCompatActivity() {

    private var exitAtSecond: Boolean = false // press back twice to exit the game
    private var screenHeight = { Resources.getSystem().displayMetrics.heightPixels } // lambda for screen height
    private var screenWidth = { Resources.getSystem().displayMetrics.widthPixels } // lambda for screen width

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setFullscreen()
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)

        MyModel.heightWindow = screenHeight()
        MyModel.widthWindow = screenWidth()

        onBackPressedDispatcher.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                if (exitAtSecond) {
                    finish()
                } else {
                    exitAtSecond = true
                    Toast.makeText(
                        this@MainActivity, "Press back again to EXIT", Toast.LENGTH_SHORT
                    ).show()
                }
                Handler(Looper.getMainLooper()).postDelayed({
                    exitAtSecond =
                        false // make it false after tot time so the user re need to press twice
                }, 2000)
            }
        })
    }

    /**
     * private fun [setFullscreen] is used to make the game in full screen.
     * it hides the navigation bar and the top bar.
     */
    private fun setFullscreen() {
        WindowCompat.setDecorFitsSystemWindows(window, false)
        WindowInsetsControllerCompat(window, findViewById(R.id.mainLayout)).let { controller ->
            controller.hide(WindowInsetsCompat.Type.systemBars())
            controller.systemBarsBehavior =
                WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE
        }
    }
}