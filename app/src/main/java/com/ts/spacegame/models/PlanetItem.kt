package com.ts.spacegame.models

/**
 * the data class [PlanetItem] is used to create planets with their name
 */
data class PlanetItem(var name: String, var image: Int)